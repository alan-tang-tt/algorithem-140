# 简要

《彤哥的算法140讲》仓库，主要放置习题链接等。

# 课程地址

课程地址：https://m.lizhiweike.com/channel2/1120766

# 博客地址

博客地址：https://www.cnblogs.com/tong-yuan/p/all.html

# 习题链接

## 第一章 基础数据结构

 1. https://leetcode-cn.com/problems/binary-tree-preorder-traversal/
 1. https://leetcode-cn.com/problems/binary-tree-inorder-traversal/
 1. https://leetcode-cn.com/problems/binary-tree-postorder-traversal/
 1. https://leetcode-cn.com/problems/binary-tree-level-order-traversal/
 1. https://leetcode-cn.com/problems/validate-binary-search-tree/
 1. https://leetcode-cn.com/problems/search-in-a-binary-search-tree/
 1. https://leetcode-cn.com/problems/construct-binary-tree-from-preorder-and-inorder-traversal/
 1. https://leetcode-cn.com/problems/construct-binary-tree-from-inorder-and-postorder-traversal/
 1. https://leetcode-cn.com/problems/implement-trie-prefix-tree/
 1. https://leetcode-cn.com/problems/concatenated-words/
 1. https://leetcode-cn.com/problems/merge-k-sorted-lists/
 1. https://leetcode-cn.com/problems/two-sum/
 1. https://leetcode-cn.com/problems/n-ary-tree-level-order-traversal/
 1. https://leetcode-cn.com/problems/valid-parentheses/
 1. https://leetcode-cn.com/problems/symmetric-tree/

## 第二章 基础算法

 1. https://leetcode-cn.com/problems/single-number/
 1. https://leetcode-cn.com/problems/single-number-iii/submissions/
 1. https://leetcode-cn.com/problems/number-of-1-bits/
 1. https://leetcode-cn.com/problems/fibonacci-number/
 1. https://leetcode-cn.com/problems/binary-search/
 1. https://leetcode-cn.com/problems/valid-perfect-square/
 1. https://leetcode-cn.com/problems/search-in-rotated-sorted-array/
 1. https://leetcode-cn.com/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof/
 1. https://leetcode-cn.com/problems/reverse-only-letters/
 1. https://leetcode-cn.com/problems/string-compression/
 1. https://leetcode-cn.com/problems/merge-two-sorted-lists/
 1. https://leetcode-cn.com/problems/minimum-difference-between-highest-and-lowest-of-k-scores/
 1. https://leetcode-cn.com/problems/dui-lie-de-zui-da-zhi-lcof/
 1. https://leetcode-cn.com/problems/sliding-window-maximum/
 1. https://leetcode-cn.com/problems/next-greater-element-i/
 1. https://leetcode-cn.com/problems/daily-temperatures/submissions/
 1. https://leetcode-cn.com/problems/trapping-rain-water/
 1. https://leetcode-cn.com/problems/running-sum-of-1d-array/
 1. https://leetcode-cn.com/problems/range-sum-query-immutable/
 1. https://leetcode-cn.com/problems/range-sum-query-2d-immutable/
 1. https://leetcode-cn.com/problems/contains-duplicate-ii/
 1. https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/
 1. https://leetcode-cn.com/problems/sliding-window-maximum/
 1. https://leetcode-cn.com/problems/beautiful-arrangement/
 1. https://leetcode-cn.com/problems/n-queens-ii/
 1. 冒泡排序
 1. 选择排序
 1. 插入排序
 1. 希尔排序
 1. 快速排序
 1. 归并排序
 1. 堆排序
 1. 桶排序
 1. 计数排序
 1. 基数排序
 1. 归纳Java源码中排序的实现方式

## 第三章 模拟与设计

 1. https://leetcode-cn.com/problems/add-strings/
 1. https://leetcode-cn.com/problems/multiply-strings/
 1. https://leetcode.cn/problems/text-justification/
 1. https://leetcode.cn/problems/min-stack/
 1. https://leetcode.cn/problems/design-add-and-search-words-data-structure/
 1. https://leetcode.cn/problems/serialize-and-deserialize-binary-tree/
 1. https://leetcode.cn/problems/lru-cache/
 1. https://leetcode.cn/problems/lfu-cache/
 1. https://leetcode.cn/problems/find-median-from-data-stream/
 1. https://leetcode.cn/problems/data-stream-as-disjoint-intervals/
 1. https://leetcode.cn/problems/insert-delete-getrandom-o1/
 1. https://leetcode.cn/problems/random-pick-with-weight/
 1. https://leetcode.cn/problems/shuffle-an-array/

 
 
 
 
 
 